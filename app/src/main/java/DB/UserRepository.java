package DB;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by cooli on 1/03/2018.
 */

public class UserRepository implements UserDataSource {

    private UserDataSource localDS;
    private static UserRepository thisInstance;

    public UserRepository(UserDataSource s){
        this.localDS=s;
    }

    public static UserRepository getInstance(UserDataSource u){
        if(thisInstance==null){
            thisInstance = new UserRepository(u);
        }
        return thisInstance;
    }


    @Override
    public Flowable<User> getUserById(long userId) {
        return localDS.getUserById(userId);
    }

    @Override
    public Flowable<List<User>> getAllUsers() {
        return localDS.getAllUsers();
    }

    @Override
    public void insertUser(User... users) {
        localDS.insertUser(users);
    }

    @Override
    public void updateUser(User... users) {
        localDS.updateUser(users);
    }

    @Override
    public void deleteUser(User user) {
        localDS.deleteUser(user);
    }

    @Override
    public void deleteAllUsers() {
        localDS.deleteAllUsers();
    }
}
