package DB;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;


/**
 * Created by cooli on 1/03/2018.
 */
@Entity(tableName = "users")
public class User {

    @PrimaryKey
    @ColumnInfo(name="id")
    private long ids;

    @ColumnInfo(name="name")
    private String names;

    @ColumnInfo(name="career")
    private String careers;


    public User(String names, long ids, String careers){
        this.names=names;
        this.ids=ids;
        this.careers=careers;
    }

    public void setIds(long name)
    {
        ids=name;
    }

    public void setNames(String name){
        names=name;
    }

    public void setCareers(String name){
        careers=name;
    }

    public long getIds(){
        return ids;
    }

    public String getNames(){
        return names;
    }

    public String getCareers(){
        return careers;
    }


}
