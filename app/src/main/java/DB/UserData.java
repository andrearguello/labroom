package DB;

import android.arch.persistence.db.SupportSQLiteOpenHelper;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.DatabaseConfiguration;
import android.arch.persistence.room.InvalidationTracker;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

/**
 * Created by cooli on 1/03/2018.
 */

@Database(entities={User.class},version=1,exportSchema = false)
public abstract class UserData extends RoomDatabase {

    @Override
    protected SupportSQLiteOpenHelper createOpenHelper(DatabaseConfiguration config) {
        return null;
    }

    @Override
    protected InvalidationTracker createInvalidationTracker() {
        return null;
    }

    public abstract UserDao userDAO();

    private static UserData thisInstance;

    public static UserData getInstance(Context context)
    {
        if(thisInstance==null){
            thisInstance= Room.databaseBuilder(context, UserData.class, "EDMT-Database-Room")
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return thisInstance;
    }
}
