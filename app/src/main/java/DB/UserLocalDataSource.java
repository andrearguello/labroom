package DB;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by cooli on 1/03/2018.
 */

public class UserLocalDataSource implements UserDataSource{

    private UserDao userDAO;
    private static UserLocalDataSource thisInstance;

    public UserLocalDataSource(UserDao userDao){
        this.userDAO=userDao;
    }

    public static UserLocalDataSource getInstance(UserDao userDao){
        if(thisInstance==null){
            thisInstance = new UserLocalDataSource(userDao);
        }
        return thisInstance;
    }

    @Override
    public Flowable<User> getUserById(long userId) {
        return userDAO.getUserById(userId);
    }

    @Override
    public Flowable<List<User>> getAllUsers() {
        return userDAO.getAllUsers();
    }

    @Override
    public void insertUser(User... users) {
        userDAO.insertUser(users);
    }

    @Override
    public void updateUser(User... users) {
        userDAO.updateUser(users);
    }

    @Override
    public void deleteUser(User user) {
        userDAO.deleteUser(user);
    }

    @Override
    public void deleteAllUsers() {
        userDAO.deleteAllUsers();
    }
}
