package DB;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by cooli on 1/03/2018.
 */

public interface UserDataSource {

    Flowable<User> getUserById(long userId);


    Flowable<List<User>> getAllUsers();


    void insertUser(User... users);


    void updateUser(User... users);


    void deleteUser(User user);


    void deleteAllUsers();
}
