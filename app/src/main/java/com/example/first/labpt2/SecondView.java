package com.example.first.labpt2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import DB.User;

public class SecondView extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second_view);

        Intent intent = getIntent();
        User example= intent.getParcelableExtra("ObjectToSend");

        long id = example.getIds();
        String name=example.getNames();
        String tex2= example.getCareers();


        TextView textView1 = findViewById(R.id.textView);
        TextView idView1= findViewById(R.id.idView);
        TextView career= findViewById(R.id.careerView);
        textView1.setText(name);
        idView1.setText(String.valueOf(id));
        career.setText(tex2);


    }
}
