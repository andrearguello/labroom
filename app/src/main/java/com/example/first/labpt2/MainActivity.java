package com.example.first.labpt2;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import DB.User;
import DB.UserData;
import DB.UserLocalDataSource;
import DB.UserRepository;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

    private List<User> thisClass;
    private UserRepository userRepo;
    private CompositeDisposable compDisp;
    ArrayAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        thisClass = new ArrayList<>();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        User user1=new User("Fulano Perez", 17801, "CCTI");
        User user2= new User("Sutano Nital",17802, "MECA");
        thisClass.add(user1);
        thisClass.add(user2);
        userRepo.insertUser(user1);
        userRepo.insertUser(user2);
        compDisp=new CompositeDisposable();

        String[] array = new String[]{String.valueOf(thisClass.get(0).getIds()), String.valueOf(thisClass.get(1).getIds()) };

        UserData db= UserData.getInstance(this);
        userRepo= UserRepository.getInstance(UserLocalDataSource.getInstance(db.userDAO()));
        loadData();

        final ListView list= findViewById(R.id.listViews);
        ArrayAdapter<String> adapt = new ArrayAdapter<>(MainActivity.this, android.R.layout.simple_list_item_1, array);
        list.setAdapter(adapt);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(MainActivity.this, SecondView.class);
                intent.putExtra("ObjectToSend", (Parcelable) thisClass.get(i));
                startActivity(intent);
            }
        });
    }

    private void loadData() {
        Disposable disposable = userRepo.getAllUsers()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<List<User>>() {
                    @Override
                    public void accept(List<User> users) throws Exception{
                        onGetAllUserSuccess(users);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Toast.makeText(MainActivity.this,""+throwable.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                });
        compDisp.add(disposable);
    }

    private void onGetAllUserSuccess(List<User> users) {
        thisClass.clear();
        thisClass.addAll(users);
        adapter.notifyDataSetChanged();
    }

}
